# Використав бібліотеку colorama, взявши звідти методи just_fix_windows_console
# та Fore - для керування кольорами тексту
import random
import sys
from colorama import Fore, just_fix_windows_console


class Card:
    SUITS = "♠ ♥ ♣ ♦".split()
    RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()

    def __init__(self, suit: str, rank: str) -> None:
        self.suit = suit
        self.rank = rank

    def __repr__(self):
        return f"{self.suit}{self.rank}"

    @property
    def value(self) -> int:
        """The value of a card is rank a number"""
        return self.RANKS.index(self.rank)

    @property
    def points(self) -> int:
        """Points this card is worth"""
        if self.suit == "♠" and self.rank == "Q":
            return 13
        elif self.suit == "♥":
            return 1
        return 0

    def __eq__(self, other: "Card") -> bool:
        """ "Compares two objects of Card for equal value"""
        if not isinstance(other, Card):
            raise TypeError(
                f"'==' not supported between instances of {type(self)} and {type(other)}"
            )
        return self.suit == other.suit and self.rank == other.rank

    def __lt__(self, other: "Card") -> bool:
        """Compares two objects of Book for less value"""
        if not isinstance(other, Card):
            raise TypeError(
                f"'==' not supported between instances of {type(self)} and {type(other)}"
            )
        return self.value < other.value


class Deck:
    def __init__(self, cards):
        self.cards = cards

    @classmethod  # method of class
    def create(cls, shuffle=False):
        """Create a new deck of 52 cards"""
        cards = [Card(s, r) for r in Card.RANKS for s in Card.SUITS]
        if shuffle:
            random.shuffle(cards)
        return cls(cards)

    def deal(self, num_hands: int):
        """Deal the cards in the deck into num_hands"""
        cls = self.__class__
        return tuple(cls(self.cards[i::num_hands]) for i in range(num_hands))


class Player:
    def __init__(self, name: str, hand: Deck) -> None:
        self.name = name
        self.hand = hand

    def play_card(self) -> None:
        """ "Play a card from the player"""
        just_fix_windows_console()
        card = random.choice(self.hand.cards)
        self.hand.cards.remove(card)
        if card.suit == "♥" or card.suit == "♦":
            print(
                Fore.LIGHTWHITE_EX + f"{self.name}:",
                Fore.LIGHTRED_EX + f"{card!r:<3}",
                end="",
            )
        else:
            print(
                Fore.LIGHTWHITE_EX + f"{self.name}:",
                Fore.LIGHTGREEN_EX + f"{card!r:<3}",
                end="",
            )


class Game:
    def __init__(self, *names: str) -> None:
        deck = Deck.create(shuffle=True)
        self.names = (list(*names) + "P1 P2 P3 P4".split())[:4]
        self.hands = {n: Player(n, h) for n, h in zip(self.names, deck.deal(4))}

    def player_order(self, start=None):
        if start is None:
            start = random.choice(self.names)
        start_idx = self.names.index(start)
        return self.names[start_idx:] + self.names[:start_idx]

    def play(self) -> None:
        """Play a card game"""
        start_player = random.choice(self.names)
        turn_order = self.player_order(start=start_player)

        while self.hands[start_player].hand.cards:
            for name in turn_order:
                self.hands[name].play_card()
            print()


if __name__ == "__main__":
    player_names = sys.argv[1:]
    game = Game(*player_names)
    game.play()

from typing import List


class Author:
    def __init__(self, first_name: str, last_name: str, year_of_birth: int) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __repr__(self) -> str:
        """Displays Author's properties in console"""
        return f"Author('{self.first_name!r}', '{self.last_name!r}', '{self.year_of_birth!r}')"

    def __str__(self) -> str:
        """Displays Author's properties for command print"""
        return f"author: {self.first_name}, {self.last_name}, birthday: {self.year_of_birth}"

    def __eq__(self, other) -> bool:
        """Compares two objects of Author for equal"""
        if not isinstance(other, Author):
            raise TypeError(
                f"'==' not supported between instances of {type(self)} and {type(other)}"
            )
        return (
            self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.year_of_birth == other.year_of_birth
        )


class Genre:
    def __init__(self, name_of_genre: str, description_of_genre: str) -> None:
        self.name_of_genre = name_of_genre
        self.description_of_genre = description_of_genre

    def __repr__(self) -> str:
        """Displays Genre's properties in console"""
        return f"Genre('{self.name_of_genre!r}', '{self.description_of_genre!r}')"

    def __str__(self) -> str:
        """Displays Genre's properties for command print"""
        return f"genre: {self.name_of_genre}, description: {self.description_of_genre}"


class Book:
    def __init__(
        self,
        name_of_book: str,
        description_of_book: str,
        authors: List[Author],
        genres: List[Genre],
        language: str,
        year_of_public: int,
        isbn: str,
    ) -> None:
        self.name_of_book = name_of_book
        self.description_of_book = description_of_book
        self.authors = authors
        self.genres = genres
        self.language = language
        self.year_of_public = year_of_public
        self.isbn = isbn

    def __repr__(self) -> str:
        """Displays Book's properties in console"""
        return (
            f"Book('{self.name_of_book!r}', '{self.description_of_book!r}', '{self.authors!r}', '{self.genres!r}', "
            f"'{self.language!r}', '{self.year_of_public!r}', '{self.isbn!r}')"
        )

    def __str__(self) -> str:
        """Displays Book's properties for command print"""
        return (
            f"book: {self.name_of_book}, description: {self.description_of_book}, authors: {self.authors},"
            f"genres: {self.genres}, language: {self.language}, year: {self.year_of_public}, isbn: {self.isbn}"
        )

    def __eq__(self, other) -> bool:
        """Compares two objects of Book for equal"""
        if not isinstance(other, Book):
            raise TypeError(
                f"'==' not supported between instances of {type(self)} and {type(other)}"
            )
        return self.name_of_book == other.name_of_book and self.authors == other.authors
